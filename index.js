var HipBot = require('hipbot');

var hipBot = new HipBot({
    jabberid: "619946_4267006@chat.hipchat.com",
    password: "Bloemkool",
    token:    "8TzaWWxxk5aSWSh65yMxbaRyRjQKMRW5DYIUzR23",
    status:   "How can I help?"
});

var featuresMentioned = true;

hipBot.hears('What is HipChat').then(function(responder, msg, matches) {
    responder.say('HipChat is the chat platform from Atlassian.');
});

hipBot.hears(/.*special.*about.*/i).then(function(responder, msg, matches) {
    responder.say('I\'ll take you on a journey.');
    responder.say('First of all, there\'s a bunch of cool features.');
});


/** Intro Movie **/
hipBot.hears(/.*before.*intro.*/i).thenSays('Sure, here\'s a small intro: https://www.youtube.com/watch?v=fcggeWCVs-0');


/** Features **/
hipBot.hears(/.*interesting.*dive deeper.*/i).then(function(responder, msg, matches) {
    responder.sayHtml('1. Public and private rooms<br/>' +
        '2. Video Conferencing<br/>' +
        '3. Persistent & Searchable chats<br/>' +
        '4. Guest Access<br/>' +
        '5. Drag and drop filesharing<br/>' +
        '6. Integrations<br /><br />' +
        '<strong>Pick one and I\'ll be happy to show you around.</strong>');

    featuresMentioned = true;
});

hipBot.hears(/.*1.*/i).then(function(responder, msg, matches) {
    if(featuresMentioned) {
        responder.sayHtml('We\'re now talking to eachother in a <strong>one-to-one chat</strong>. It\'s just you and me, and some virtual candle light.<br />' +
            'You can also create a <strong>private room</strong> where you can decide which friends or colleagues you can chat with<br />' +
            'And ofcourse you can also create many <strong>public rooms</strong> which anyone, who has access to your HipChat, can join.');

        // Send a message to a public room
        hipBot.getHipchatterInstance().send_message('Public Room',
            {message: '@here Does anyone know how to subscribe to the Atlassian Newsletter? (Pssst: atlassian.com/newsletter'},
            function(err){
                if (err == null) console.log('Successfully send message to the room.');
                else console.log('Error', err);
            });

        setTimeout(function(){
            responder.say('I\'ve tried sending a message to "Private Room" but I don\'t seem to have access :-( Please invite me!');
        }, 200);
    }
});

hipBot.hears(/.*been invited.*/i).then(function(responder, msg, matches) {
    responder.say('Thanks my friend! (awthanks)');
    // Send a message to a private room
    hipBot.getHipchatterInstance().send_message('Private Room',
        {message: '@here Wanna get a drink tonight?'},
        function(err){
            if (err == null) console.log('Successfully send message to the room.');
            else console.log('Error', err);
        });
});

hipBot.hears(/.*2.*/i).then(function(responder, msg, matches) {
    if(featuresMentioned) {
        responder.say('I\'m a bot, I can\'t video chat (shrug).');
        setTimeout(function(){
            responder.say('But I stole some images from the blog that Atlassian wrote two years ago: https://blog.hipchat.com/2014/05/06/hipchat-video-and-screen-sharing-are-here/');
        }, 200);
        setTimeout(function(){
            responder.say('https://s3.amazonaws.com/uploads.hipchat.com/10804/61803/MJgcpx23ymagAy4/HipChat_Video.png');
        }, 400);
        setTimeout(function(){
            responder.say('https://s3.amazonaws.com/uploads.hipchat.com/10804/61803/VwLcYuyiuIrOMzN/Screenshare.png');
        }, 400);
    }
});

hipBot.hears(/.*3.*/i).then(function(responder, msg, matches) {
    if(featuresMentioned) {
        responder.say('All messages are stored somewhere in the cloud and you can even search them!');
    }
});

hipBot.hears(/.*4.*/i).then(function(responder, msg, matches) {
    if(featuresMentioned) {
        responder.sayHtml('(owyeah) Guest access is so cool. It\'ll allow you to open up your HipChat room to people without an account!');
        hipBot.getHipchatterInstance().create_room({
            name: 'Questions',
            topic: 'If you have any questions, feel free to post them here during the talk!',
            "guest_access": true
        },
        function (err, room) {
            if (err == null) {
                // Send message to new room "Questions"
                hipBot.getHipchatterInstance().send_message('Questions',
                    {message: '@MaartenCautreels Here\'s the new room, guest access is already enabled.'},
                    function(err){
                        if (err == null){
                            // Share Guest Access link to the new room "Questions"
                            hipBot.getHipchatterInstance().get_room(room.entity.id, function (err, room) {
                                if (err == null) {
                                    console.log(room);
                                    responder.sayHtml('<strong>Join the room via:</strong> <a href="' + room['guest_access_url'] + '">' + room['guest_access_url'] + '</a>');
                                } else {
                                    console.log('Error', err);
                                }
                            });
                        } else {
                            console.log('Error', err);
                        }
                    });
            } else {
                console.log('Error', err);
            }
        });
    }
});

hipBot.hears(/.*5.*/i).then(function(responder, msg, matches) {
    if(featuresMentioned) {
        responder.sayHtml('I don\'t use a mouse, I only use my keyboard so I never drag and drop files, but feel free to give it a try. I guarantee you it works!<br />' +
            'HipChat supports files of <strong>any type</strong> up to <strong>50MB</strong>.');

    }
});

hipBot.hears(/.*6.*/i).then(function(responder, msg, matches) {
    if(featuresMentioned) {
        responder.say('Integrate (allthethings)! More specifics: https://www.hipchat.com/integrations');
    }
});

/** Other cool stuff **/
hipBot.hears(/.*enough about features.*/i).then(function(responder, msg, matches) {
    featuresMentioned = false;
    hipBot.getHipchatterInstance().send_message('Presentation Prep',
        {message: '@MaartenCautreels Alright, you can also mention people in a room if you want to ask them something directly. You can also use @here and @all to spam everyone in the room! It\'s quite fun!'},
        function(err){
            if (err == null) console.log('Successfully send message to the room.');
            else console.log('Error', err);
        });
});

hipBot.hears(/.*emoticons.*/i).then(function(responder, msg, matches) {
    if(!featuresMentioned) {
        responder.say('Oh man, there\'s so many emoticons. Here\'s a few: (allthethings)(android)(areyoukiddingme)(arrington)(arya)(bitbucket)(confluence)(donotwant)(feelsgoodman)(hipchat)(mindblown) If you want the full list you should visit https://www.hipchat.com/emoticons');
    }
});

hipBot.hears(/.*tips.*developers.*/i).then(function(responder, msg, matches) {
    if(!featuresMentioned) {
        responder.say('Developers <3 HipChat. You can add code samples using /code');
        setTimeout(function(){
            responder.say('/code ' +
                '\nif(question === "What\'s the answer to Life, the Universe and Everything?") {' +
                '\n\treturn 42;' +
                '\n}');
        }, 200);
    }
});

/** Generic Messages **/
var numberOfYes = 0;
var yesAnswers = ['Ok let\'s get started', 'We\'re on the go'];

hipBot.hears('Yes').then(function(responder, msg, matches) {
    responder.say(yesAnswers[numberOfYes]);
    numberOfYes++;
});

var sorryAnswers = ['No problem my friend, just give it another try.', 'Don\'t worry, just try again.', 'It\'s ok'];

hipBot.hears(/.*sorry.*/i).then(function(responder, msg, matches) {
    responder.say(sorryAnswers[Math. floor(Math.random() * sorryAnswers.length)]);
});

hipBot.hears().thenSays("You probably made a typo!");
